#!/usr/bin/env python
# -*- coding: utf-8 -*-
import datetime
import matplotlib.pyplot as plt
import numpy as np
from db_connect import Account, Tweet, Words, Activity
from collections import OrderedDict


# 10 самых популярных слова
def words(show=False):
    data = Words.select().order_by(-Words.count).limit(10)
    bar = OrderedDict()
    for item in data:
        bar[item.word] = int(item.count)
    get_graph('Рейтинг слов', 'Слова', 'Кол-во', bar, rotation=45, show=show)
    return bar


# количество твитов за пол часа, и выбор аккаунта
def count_tweets(account, show=False):
    data = Tweet.select().join(Account).where(Account.name == str(account))
    if len(data) == 0:
        print('Graph.count_tweets: no data - return')
        return
    bar = []
    # datetime.timedelta([days,] [seconds,] [microseconds,] [milliseconds,] [minutes,] [hours,] [weeks])
    for item in data:
        bar.append(item.created)
    bar.sort()
    half_hour = datetime.timedelta(minutes=30)
    result = time_delta(bar, half_hour)
    get_graph('Выборка периодов', 'Периоды (30 минут)', 'Кол-во твитов', result, fontsize=8, show=show)
    return result


def time_delta(data, delta=datetime.timedelta(hours=1)):
    result = OrderedDict()
    date_format = '%H:%M'
    date = data[0] + delta
    item = 0
    while item < len(data):
        if data[item] <= date:
            if not date.strftime(date_format) in result:
                result[date.strftime(date_format)] = 0
            result[date.strftime(date_format)] += 1
            item += 1
        else:
            date = date + delta
    return result


# активность аккаунтов по количеству твитов
def tweeter_activity(show=False):
    data = Activity.select().join(Account).where(Activity.account == Account.id).order_by(-Activity.count_tweets)
    bar = OrderedDict()
    for item in data:
        bar[item.account.name] = item.count_tweets
    get_graph('Активность', 'Логин', 'Кол-во твитов', bar, fontsize=4, show=show)
    return bar


# построить график
def get_graph(title='Test', y_lab='y', x_lab='x', data=None, fontsize=14, rotation=0, show=False):

    if data is None:
        data = {'A': 50, 'B': 10, 'C': 200, 'D': 100}

    x = []
    l = []
    for key, value in data.items():
        l.append(key)
        x.append(value)

    figure = plt.figure()
    axes = figure.add_axes()
    plt.title(title)
    plt.xlabel(x_lab)
    plt.ylabel(y_lab)
    # plt.xticks()
    # plt.yticks()
    plt.grid(True)

    y = np.arange(len(x))
    plt.barh(y, x, align='center')
    plt.yticks(y, l, fontsize=fontsize, rotation=rotation)
    if show:
        plt.show()
    else:
        plt.savefig('tweeter/tweeter/static/' + title + '.png', fmt='png')
    return plt, figure

if __name__ == '__main__':
    get_graph()
    # words()
    # tweeter_activity()
    # count_tweets('ArianaGrande')
