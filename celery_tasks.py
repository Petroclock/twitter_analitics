# -*- coding: utf-8 -*-
from celery import Celery
import twitter_api

app = Celery('celery_tasks', broker='amqp://guest:guest@localhost:5672//', backend='rpc://')
app.conf.beat_schedule = {
    'schedule-main': {
        'task': 'celery_tasks.main',
        'schedule': 600.0
    },
}
'''
@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    # Calls test('hello') every 10 seconds.
    sender.add_periodic_task(10.0, test.s('hello'), name='add every 10')

    # Calls test('world') every 30 seconds
    sender.add_periodic_task(30.0, test.s('world'), expires=10)
'''


@app.task
def test():
    print('Start')
    return 'Success'


@app.task
def main():
    print('------PARSE TWEETER : RUN------')
    twitter_api.main()
    return True
