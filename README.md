### Порядок и список команд:  
**sudo rabbitmqctl start_app** (stop_app - остановить) - запустить брокер (бэкенд)  
**celery -A celery_tasks worker -E -B --loglevel=info** - запустить очередь заданий  
**python3 manage.py runserver** - запустить сервер Flask  
**python3 -m unittest unit_test.py** - тесты  

### Описание ключевых файлов:  
**/tweeter** - папка с проектом Flask  
**celery_tasks.py** - Celery  
**db_connect.py** - объявление модели бд (запустить для создания бд если нету)  
**graph.py** - модуль для постройки графиков  
**manage.py** - запуск сервера Flask  
**twitter_api.py** - функции для обработки данных с твитера  
**unit_test.py** - тесты  
**notebook.ipynb** - ipython notebook с аналитичекими запросами  
**peewee.db** - файл базы  

### Образ Docker
**docker pull petroclock/tweeter**  
  
### Описание  
Сервис периодического скачивания сообщений с аккаунтов твиттера.  
Скачивание организовано очередью заданий в связке Celery + RabbitMQ  
Работа с твиитером организована с помощью tweepy  
Результат работы сохраняется в базе данных peewee  
Построение графика matplotlib  
Результат посмотреть в Flsk  
![Активность.png](https://bitbucket.org/repo/goRLRR/images/2397438379-%D0%90%D0%BA%D1%82%D0%B8%D0%B2%D0%BD%D0%BE%D1%81%D1%82%D1%8C.png)  
![Выборка периодов.png](https://bitbucket.org/repo/goRLRR/images/2270323275-%D0%92%D1%8B%D0%B1%D0%BE%D1%80%D0%BA%D0%B0%20%D0%BF%D0%B5%D1%80%D0%B8%D0%BE%D0%B4%D0%BE%D0%B2.png)  
![Рейтинг слов.png](https://bitbucket.org/repo/goRLRR/images/4182563399-%D0%A0%D0%B5%D0%B9%D1%82%D0%B8%D0%BD%D0%B3%20%D1%81%D0%BB%D0%BE%D0%B2.png)