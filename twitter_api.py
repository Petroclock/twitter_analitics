# -*- coding: utf-8 -*-
import tweepy
from db_connect import *
import graph

list_akk = ["katyperry",
            "justinbieber",
            "BarackObama",
            "taylorswift13",
            "YouTube",
            "rihanna",
            "ladygaga",
            "jtimberlake",
            "TheEllenShow",
            "twitter",
            "britneyspears",
            "instagram",
            "Cristiano",
            "KimKardashian",
            "shakira",
            "JLo",
            "selenagomez",
            "ArianaGrande",
            "ddlovato",
            "cnnbrk",
            "jimmyfallon",
            "Oprah",
            "Pink",
            "Harry_Styles",
            "Drake",
            "onedirection",
            "BillGates",
            "LilTunechi",
            "KingJames",
            "KAKA",
            "BrunoMars",
            "NiallOfficial",
            "aliciakeys",
            "MileyCyrus",
            "espn",
            "pitbull",
            "KevinHart4real",
            "wizkhalifa",
            "Real_Liam_Payne",
            "Louis_Tomlinson",
            "NICKIMINAJ",
            "SportsCenter",
            "Eminem",
            "ActuallyNPH",
            "neymarjr",
            "CNN",
            "AvrilLavigne",
            "nytimes"]


# Подключение
def tweet_connect():
    app_key = {
        'consumer_key': 'DTcI2i7vJvEKm3Yp9bAddEDlQ',
        'consumer_secret': '1660Hifbjs8bK0evxX0B3qUZ8EG5IcnwNyy0O2GxILShXCL7vi',
        'access_token': '3367700621-NJgjeaoo3CQwBKtcHUbnrs0hqPb8BrXAPlFTCiN',
        'access_token_secret': 'XLJV904MNIhZJxJqeGzvYhPpUqeO7v6x6rETIba2pQyyP'
    }
    auth = tweepy.OAuthHandler(app_key['consumer_key'], app_key['consumer_secret'])
    auth.set_access_token(app_key['access_token'], app_key['access_token_secret'])
    print('Connect Tweeter: Success')
    return tweepy.API(auth)


# Проверка текста
def check_text(text):
    if type(text) != str:
        return False
    if len(text) == 0:
        return False
    return True


# Очистить текст
def clear_text(text):
    bad_word = 0
    bad_list = []
    if not check_text(text):
        return
    tmp = ['http', '@', '#', '<', '>', '&', '$', '-', '—']
    chars = [',', '.', '/', ' ', '!', '?', ':', ';', '(', ')', '\'', '’', '…', '"', '\n', '\t', '”', '“', '‘', '[', ']']
    text = text.split(' ')
    result = []
    for index, item in enumerate(text):
        for char in chars:
            item = item.replace(char, '')
        if item.isdigit():
            continue
        for temp in tmp:
            if item.find(temp) != -1:
                item = ''
                break
        if len(item) > 0 and item.isalpha():
            item = item.capitalize()
            result.append(item)
        elif not item.isalpha() and len(item) > 0:
            # print(item, item.isalpha())
            # bad_word += 1
            bad_list.append(item)
    # print('Good words: ', len(result))
    # print('Bad words: ', len(bad_list))
    return ' '.join(result)


# Посчитать слова
def word_count(text):
    if not check_text(text):
        return
    text = text.split(' ')
    count = {}
    for word in text:
        if word in count:
            count[word] += 1
        else:
            count[word] = 1
    return count


# Сохранить твит
def save_tweet(account, msg, created, msg_id):
    if not check_tweet_duplicate(msg_id) or len(msg) == 0:
        return
    tweet = Tweet.create(account_id=int(account), msg=msg, created=created, msg_id=int(msg_id))
    return tweet


# Сохранить активность
def save_activity(account, count_tweets):
    activity = Activity.select().where(Activity.account == account)
    if len(activity) == 0:
        activity = Activity(account_id=int(account), count_tweets=int(count_tweets))
    elif len(activity) == 1:
        activity = activity[0]
        activity.count_tweets += int(count_tweets)
    activity.save()
    return activity


# Сохранить слова и их кол-во повторений
def save_words(text):
    if not check_text(text):
        return
    words = []
    count_word = word_count(text)
    for msg, count in count_word.items():
        word = Words.select().where(Words.word == msg)
        if len(word) == 0:
            word = Words(word=msg, count=int(count))
        elif len(word) == 1:
            word = word[0]
            word.count += count
        word.save()
        words.append(word)
    return words


# Получить список аккаунтов
def get_accounts_list():
    accounts = Account.select().where(Account.id > 0)
    if len(accounts) == 0:
        accounts = []
        for acc_name in list_akk:
            acc_obj = Account.create(name=acc_name)
            accounts.append(acc_obj)
    print('List account: ', len(accounts))
    return accounts


# Пропарсить твиты
def parse_tweets(tweets):
    account = []
    for tweet in tweets:
        # print tweet.id, tweet.text, tweet.created_at
        if check_tweet_duplicate(tweet.id):
            account.append({'id': tweet.id, 'text': clear_text(tweet.text), 'created': tweet.created_at})
    return account


# Пропарсить аккаунты
def parse_accounts(api):
    data = {}
    list_account = get_accounts_list()
    for account in list_account:
        try:
            line = api.user_timeline(account.name)
        except:
            print(account.name)
            raise
        print('Processing: ', account.name, ' count tweets: ', len(line))
        data[account.id] = parse_tweets(line)
    return data


# Проверка на существующие в базе твиты и подлежащие записи
def check_tweet_duplicate(tweet_id):
    record = Tweet.select().where(Tweet.msg_id == tweet_id)
    if len(record) > 0:
        return False
    return True


def main():
    api = tweet_connect()
    accounts = parse_accounts(api)
    for key, value in accounts.items():
        for item in value:
            save_words(item['text'])
            save_tweet(account=key, msg=item['text'], created=item['created'], msg_id=item['id'])
        save_activity(account=key, count_tweets=len(accounts[key]))
    graph.words()
    graph.tweeter_activity()
    print('------PARSE TWEETER : DONE------')
    return


if __name__ == '__main__':
    main()
