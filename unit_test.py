# -*- coding: utf-8 -*-
import unittest
import twitter_api
import tweepy
from db_connect import User, Tweet, Account, Words, Activity
import datetime
from peewee import SelectQuery
import celery_tasks
from celery.result import AsyncResult
import graph
import matplotlib.pyplot as plt
from tweeter.tweeter.app import create_app
from lxml.html import fromstring


@unittest.skip('Run: Celery, RabbitMQ')
class TestCeleryTasks(unittest.TestCase):
    def test_task(self):
        result = celery_tasks.test.apply_async(serializer='json')
        self.assertIsInstance(result, AsyncResult)
        self.assertIsInstance(result.get(timeout=5), str)
        self.assertTrue(result.successful())


class TestTwitterApi(unittest.TestCase):
    def test_tweet_connect(self):
        result = twitter_api.tweet_connect()
        self.assertIsInstance(result.me(), tweepy.models.User)
        return

    def test_check_text(self):
        self.assertTrue(twitter_api.check_text('test'))
        self.assertTrue(twitter_api.check_text(u'text'))
        self.assertFalse(twitter_api.check_text([]))
        self.assertFalse(twitter_api.check_text(''))
        self.assertFalse(twitter_api.check_text(u''))
        return

    def test_check_tweet_duplicate(self):
        record = Tweet.select().where(Tweet.id > 0)
        if len(record) > 0:
            self.assertFalse(twitter_api.check_tweet_duplicate(record[0].msg_id))
        else:
            record = Tweet.create(account_id=1, msg='test', created=datetime.datetime.now(), msg_id=1)
            self.assertFalse(twitter_api.check_tweet_duplicate(record.msg_id))
            record.delete_instance()
        self.assertTrue(twitter_api.check_tweet_duplicate(0))
        return

    def test_clear_text(self):
        result = twitter_api.clear_text(' test http://test.com/test 1204, TEST! @test me. #test_test ')
        self.assertEqual(result, 'Test Test Me')
        return

    def test_word_count(self):
        result = twitter_api.word_count('t e s t')
        test = {'t': 2, 'e': 1, 's': 1}
        self.assertEqual(result, test)
        return

    def test_save_tweet(self):
        result = twitter_api.save_tweet(account=1, msg='test', created=datetime.datetime.now(), msg_id=1)
        if not result:
            return
        self.assertIsInstance(result, Tweet)
        count = result.delete_instance()
        self.assertEqual(count, 1)
        return

    def test_save_activity(self):
        result = twitter_api.save_activity(account=1, count_tweets='1')
        self.assertIsInstance(result, Activity)
        count = result.delete_instance()
        self.assertEqual(count, 1)
        return

    def test_save_word(self):
        result = twitter_api.save_words('test')
        self.assertEqual(len(result), 1)
        for word in result:
            self.assertIsInstance(word, Words)
            count = word.delete_instance()
            self.assertEqual(count, 1)
        return

    def test_get_account_list(self):
        result = twitter_api.get_accounts_list()
        self.assertEqual(type(result), SelectQuery)
        self.assertEqual(len(result), 48)
        return

    def test_parse_tweets(self):
        data = [Tweet()]
        data[0].id = 1
        data[0].text = 'test'
        data[0].created_at = datetime.datetime.now()
        result = twitter_api.parse_tweets(data)
        self.assertIsInstance(result, list)
        self.assertTrue(len(result) == 1)
        self.assertIsInstance(result[0], dict)
        self.assertTrue('id' in result[0])
        self.assertTrue('text' in result[0])
        self.assertTrue('created' in result[0])
        return

    def test_parse_account(self):
        api = twitter_api.tweet_connect()
        result = twitter_api.parse_accounts(api)
        self.assertIsInstance(result, dict)
        for account in Account.select():
            self.assertTrue(account.id in result)
            if account.id in result:
                for tweet in result[account.id]:
                    self.assertTrue(len(tweet) > 0)
        return


class TestGraph(unittest.TestCase):
    def check_dict(self, result):
        self.assertIsInstance(result, dict)
        for key, val in result.items():
            self.assertIsInstance(val, int)
        return

    def test_words(self):
        row = Words.create(word='test', count=1)
        result = graph.words()
        self.check_dict(result)
        row.delete_instance()
        return

    def test_count_tweets(self):
        account = Account.create(name='test')
        tweet = Tweet.create(account=account.id,
                             msg='test',
                             created=datetime.datetime.now(),
                             msg_id=1)
        result = graph.count_tweets(account.name)
        self.check_dict(result)
        tweet.delete_instance()
        account.delete_instance()
        return

    def test_range_date(self):
        data = []
        start = datetime.datetime.now()
        delta = datetime.timedelta(minutes=30)
        for item in range(3):
            data.append(start + delta)
        result = graph.time_delta(data, delta)
        self.assertEqual(len(result.items()), 1)
        for key, val in result.items():
            self.assertEqual(val, 3)
        return

    def test_tweeter_activity(self):
        account = Account.create(name='test')
        activity = Activity.create(account=account.id, count_tweets=1)
        result = graph.tweeter_activity()
        self.check_dict(result)
        activity.delete_instance()
        account.delete_instance()
        return

    def test_get_graph(self):
        obj, figure = graph.get_graph()
        self.assertEqual(obj, plt)
        return


class TestFlask(unittest.TestCase):
    def setUp(self):
        self.app = create_app()
        self.app.testing = True
        self.app.config['TESTING'] = True
        self.client = self.app.test_client()
        self.success = 'alert alert-success'
        self.warning = 'alert alert-warning'
        self.err_msg = ['Username - This field is required.',
                        'Password - This field is required.',
                        'CSRF Token - The CSRF token is missing.',
                        'CSRF Token - The CSRF token is invalid',
                        'Username - Unknown username',
                        'Password - Invalid password']
        self.suc_log = ['You are logged in']
        self.suc_out = ['You are logged out']
        self.login_dict = [{'username': 'login', 'password': 'password', 'result': True},
                           {'username': 'user', 'password': 'password', 'result': False},
                           {'username': 'login', 'password': 'user', 'result': False},
                           {'username': '', 'password': '', 'result': False}]
        self.register_dict = [{'username': 'new_user', 'password': 'test_pass', 'confirm': 'test_pass', 'result': True},
                              {'username': 'login', 'password': 'password', 'confirm': 'password', 'result': False},
                              {'username': 'new_user', 'password': 'test_pass', 'confirm': '', 'result': False},
                              {'username': 'new_user', 'password': '', 'confirm': 'test_pass', 'result': False},
                              {'username': '', 'password': 'test_pass', 'confirm': 'test_pass', 'result': False},
                              {'username': 'new_user', 'password': 'test', 'confirm': 'test', 'result': False}]
        self.views = ['/', '/register/', '/about/']
        self.csrf_token = '119542b83c42b5e054fb823210605a98cbd6c824'

    def parsing_html(self, post):
        result = post.data.decode('utf8')
        page = fromstring(result)
        warning = page.find_class(self.warning)
        success = page.find_class(self.success)
        if len(warning) > 0:
            # print('Warning: ', len(warning))
            self.parse_msg(warning)
            return False
        elif len(success) > 0:
            # self.parse_msg(success)
            # print('Success: ', len(success))
            return True
        else:
            return 'No message'

    def parse_msg(self, content, messages=None):
        for item in content:
            print('Message: ', item.text_content())
            # for msg in messages:
            # if item.text_content().find(msg) >= 0:
            # return True
        return

    def test_view(self):
        for view in self.views:
            request = self.client.get(view)
            self.assertEqual(request.status_code, 200)
        return

    def test_post(self):
        with self.app.test_client() as c:
            self.home()
            self.register()
        return

    def home(self):
        for item in self.login_dict:
            post = self.client.post('/', data=dict(username=item['username'], password=item['password']),
                                    follow_redirects=True)
            self.assertEqual(self.parsing_html(post), item['result'])
            if item['result']:
                request = self.client.get('/logout/', follow_redirects=True)
                self.assertEqual(request.status_code, 200)
        return

    def register(self):
        for item in self.register_dict:
            request = self.client.post('/register/', data=dict(username=item['username'],
                                                               password=item['password'],
                                                               confirm=item['confirm']),
                                       follow_redirects=True)
            try:
                self.assertEqual(self.parsing_html(request), item['result'])
            except AssertionError:
                print(request)
                raise AssertionError
            if item['result']:
                for row in User.select().where(User.login == 'new_user'):
                    row.delete_instance()
        return


if __name__ == '__main__':
    unittest.main()
