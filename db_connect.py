# -*- coding: utf-8 -*-
from peewee import *
from playhouse.db_url import connect

# db = MySQLDatabase('tweeter', user='player', password='testweet')
db = connect('mysql://player:testweet@localhost/tweeter')


class User(Model):
    is_authenticated = True
    login = CharField()
    password = CharField()
    created = DateTimeField()

    class Meta:
        databese = db


class Account(Model):
    name = CharField()

    class Meta:
        databese = db


class Tweet(Model):
    account = ForeignKeyField(Account)
    msg = TextField()
    created = DateTimeField()
    msg_id = IntegerField()

    class Meta:
        databese = db


class Words(Model):
    word = CharField()
    count = IntegerField()

    class Meta:
        databese = db


class Activity(Model):
    account = ForeignKeyField(Account)
    count_tweets = IntegerField()

    class Meta:
        databese = db


if __name__ == '__main__':
    try:
        db.create_tables([User, Account, Tweet, Words, Activity])
        print('Tables created')
    except OperationalError:
        print('Tables already exists')
