# -*- coding: utf-8 -*-
"""Public forms."""
from flask_wtf import Form
from wtforms import PasswordField, StringField
from wtforms.validators import DataRequired
from tweeter.tweeter.extensions import csrf_protect

from db_connect import User


@csrf_protect.exempt
class LoginForm(Form):
    """Login form."""

    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])

    def __init__(self, *args, **kwargs):
        """Create instance."""
        super(LoginForm, self).__init__(*args, **kwargs)
        self.user = None

    def validate(self):
        """Validate the form."""
        initial_validation = super(LoginForm, self).validate()
        if not initial_validation:
            return False

        self.user = User.select().where(User.login == self.username.data)
        if len(self.user) == 0:
            self.username.errors.append('Unknown username')
            return False
        else:
            self.user = self.user[0]

        if self.user.password != self.password.data:
            self.password.errors.append('Invalid password')
            return False
        return True
