# -*- coding: utf-8 -*-
"""Public section, including homepage and signup."""
from flask import Blueprint, flash, redirect, render_template, request, url_for, session
from flask_login import login_required, logout_user
from db_connect import User, Account
from tweeter.tweeter.extensions import login_manager
from tweeter.tweeter.public.forms import LoginForm
from tweeter.tweeter.user.forms import RegisterForm
from tweeter.tweeter.utils import flash_errors
from tweeter.tweeter.extensions import csrf_protect
import datetime
import graph

blueprint = Blueprint('public', __name__, static_folder='../static')


@login_manager.user_loader
def load_user(user_id):
    """Load user by ID."""
    result = User.select().where(User.id == int(user_id))
    if len(result) > 0:
        return result[0]


@blueprint.route('/', methods=['GET', 'POST'])
@csrf_protect.exempt
def home():
    """Home page."""
    form = LoginForm(request.form)
    # Handle logging in
    if request.method == 'POST':
        if form.validate():
            form.user.is_authenticated = True
            login(form.user)
            flash('You are logged in', 'success')
            redirect_url = request.args.get('next') or url_for('user.members')
            return redirect(redirect_url)
        else:
            flash_errors(form)
    return render_template('public/home.html', form=form)


def login(user, remember=False, fresh=True):
    session['user_id'] = user.id
    session['_fresh'] = fresh
    session['_id'] = user.id
    if remember:
        session['remember'] = 'set'
    return True


@blueprint.route('/logout/')
@login_required
def logout():
    """Logout."""
    logout_user()
    flash('You are logged out', 'info')
    return redirect(url_for('public.home'))


@blueprint.route('/register/', methods=['GET', 'POST'])
def register():
    """Register new user."""
    form = RegisterForm(request.form, csrf_enabled=False)
    if form.validate:
        User.create(login=form.username.data, password=form.password.data, created=datetime.datetime.now())
        flash('Thank you for registering. You can now log in.', 'success')
        return redirect(url_for('public.home'))
    else:
        flash_errors(form)
    return render_template('public/register.html', form=form)


@blueprint.route('/about/')
def about():
    """About page."""
    form = LoginForm(request.form)
    return render_template('public/about.html', form=form)


@blueprint.route('/members', methods=['GET', 'POST'])
@login_required
def get_graph():
    row = Account.select()
    accounts = []
    for item in row:
        accounts.append(item.name)
    if request.method == 'POST':
        req = request.form
        flash(req['account'], 'success')
        graph.count_tweets(req['account'], show=True)
    return render_template('users/members.html', accounts=accounts)
