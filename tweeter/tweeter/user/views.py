# -*- coding: utf-8 -*-
"""User views."""
from flask import Blueprint, render_template, request, flash
from flask_login import login_required
import db_connect

blueprint = Blueprint('user', __name__, url_prefix='/users', static_folder='../static')


@blueprint.route('/', methods=['GET', 'POST'])
@login_required
def members():
    row = db_connect.Account.select()
    accounts = []
    for item in row:
        accounts.append(item.name)
    if request.method == 'POST':
        req = request.form
        flash(req['account'], 'success')
    return render_template('users/members.html', accounts=accounts)
